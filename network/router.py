from components.property.network import property_network
from .response import error_response


def router(request, data):
    try:
        route = "/" + request.path.split("/")[1]
        complement = request.path.split("/")[2].split("?")[0]
        switch_router(route, request, data, complement)
    except Exception as e:
        print("Router Error: ", e)
        error_response(request, 'NO_VALID_DATA')


def switch_router(route, request, data, complement):
    def property_():
        property_network(request, data, complement)

    switch_dict = {
        '/property': property_,
    }

    return switch_dict.get(route)()
