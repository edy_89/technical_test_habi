from .response import error_response
from urllib.parse import urlparse
from functools import wraps
from config import AUTHCODE


def auth_header():
    def _auth_header(view_func):
        def __auth_header(request, *args, **kwargs):
            """validate authcode sended throug headers"""
            authcode = request.headers.get('AUTHCODE')

            if not authcode or authcode != AUTHCODE:
                error_response(request, 'NOT_AUTH_APP')
                return

            return view_func(request, *args, **kwargs)
        return wraps(view_func)(__auth_header)
    return _auth_header


def validate_data_header():
    def _validate_data_header(view_func):
        def __validate_data_header(request, *args, **kwargs):
            """validate data sended throug headers"""
            data = {}

            try:
                params = [param.split("=") for param in urlparse(
                    request.path).query.split("&")]

                if params[0] != ['']:
                    for key, value in params:
                        data[key] = value

            except Exception as e:
                print("Decorator Error: ", e)
                error_response(request, 'NO_VALID_DATA')
                return

            return view_func(request, data=data, *args, **kwargs)
        return wraps(view_func)(__validate_data_header)
    return _validate_data_header
