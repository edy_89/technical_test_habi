import json

STATUS_CODES = {
    "OK": 200,
    "CREATED": 201,
    "NOT_FOUND": 404,
    "BAD_REQUEST": 400,
    "UNATHORIZED": 403,
    "METHOD_NOT_ALLOWED": 405
}

API_ERRORS = {
    "NOT_AUTH_APP": {
        "err_code": "NOT_AUTH_APP",
        "description:": "Auth code incorrect or does not exist",
        "status_code": STATUS_CODES["UNATHORIZED"]
    },
    "NOT_FOUND": {
        "err_code": "NOT_FOUND",
        "description:": "URL called does not exist",
        "status_code": STATUS_CODES["NOT_FOUND"]
    },
    'NO_VALID_DATA': {
        'err_code': 'NO_VALID_DATA',
        'description:': 'No valid data',
        "status_code": STATUS_CODES['BAD_REQUEST']
    }
}


def success_response(server, status_code, data):
    server.send_response(STATUS_CODES[status_code])
    server.do_HEAD()
    server.wfile.write(json.dumps(data).encode("utf-8"))
    return


def error_response(server, error):
    server.send_response(API_ERRORS[error]["status_code"])
    server.do_HEAD()
    server.wfile.write(json.dumps(API_ERRORS[error]).encode("utf-8"))
    return
