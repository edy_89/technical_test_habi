import pymysql
from config import DB, HOST, PORT, USER, PASS


def connect_db():
    try:
        connection = pymysql.connect(
            db=DB,
            host=HOST,
            port=int(PORT),
            user=USER,
            password=PASS,
            cursorclass=pymysql.cursors.DictCursor
        )

        cursor = connection.cursor()

        print("Database connection success!")

        return connection, cursor
    except:
        print("Database connection failed!")


def end_connect_db(connection):
    connection.close()
    print("Database connection have been closed!")
