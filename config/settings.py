import os
from dotenv import load_dotenv

load_dotenv()

AUTHCODE = os.getenv('AUTHCODE')

DB = os.getenv('DB')
HOST = os.getenv('HOST')
PORT = os.getenv('PORT')
USER = os.getenv('USER')
PASS = os.getenv('PASS')
