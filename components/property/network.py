from network.response import error_response, success_response
from .store import get_records_from_db
from utils import build_query_string


def property_network(request, data, complement):

    if complement == 'single':
        if not data or not data["id"]:
            error_response(request, 'NO_VALID_DATA')
        else:
            query_string = build_query_string(data)
            response = get_records_from_db(query_string)
            success_response(request, 'OK', response)

    if complement == 'many':
        if not data:
            response = get_records_from_db()
            success_response(request, 'OK', response)
        else:
            query_string = build_query_string(data)
            response = get_records_from_db(query_string)
            success_response(request, 'OK', response)
