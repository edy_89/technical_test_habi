from config import connect_db, end_connect_db
from utils import prune_query


def get_records_from_db(query_string=None):
    connection, cursor = connect_db()

    records_query = f"""
    SELECT PRO.id,PRO.address,PRO.city,STA.label,PRO.price,PRO.description,PRO.year
    FROM property PRO
        INNER JOIN status_history STH
        ON PRO.id = STH.property_id
        INNER JOIN status STA
        ON STA.id = STH.status_id
        WHERE (
                PRO.address != 'None' AND PRO.city != 'None'  AND PRO.price != 'None'
                {query_string if query_string else ''}
                AND STA.name not in ('comprando', 'comprado')
                AND Exists(
                    SELECT *
                    FROM   status_history
                    WHERE  property_id = STH.property_id
                    HAVING STH.update_date = Max(update_date)
                )
            )
    ORDER BY PRO.id
    """

    cursor.execute(records_query)

    query = prune_query([prop for prop in cursor.fetchall()])

    end_connect_db(connection)

    return query
