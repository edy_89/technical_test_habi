import json


def prune_query(query):
    for record in query:
        for key in record.keys():
            if record[key] in [None, '', 'empty']:
                record[key] = "no registra"

    return json.loads(str(query).replace("'", '"'))


def build_query_string(data):
    query_string = ""
    for key in data.keys():
        if key in ["id", "year", "city"]:
            query_string += f"AND PRO.{key}='{data[key]}' "
        if key == "state":
            query_string += f"AND STA.name='{data[key]}' "
    return query_string
