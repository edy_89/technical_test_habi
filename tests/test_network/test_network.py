from network import router

class SimulatorServerTest:
    def __init__(self,path):
        self.path = path

    def do_HEAD(self):
        pass
    
    def send_response(self,status_code):
        pass

    @property
    def wfile(data):
        write = data
        return write

    def write(self,data):
        return data

def test_router():
    server_1 = SimulatorServerTest("/property/many?city=medellin&year=2002")
    server_2 = SimulatorServerTest("/property/single?id=123")
    server_3 = SimulatorServerTest("/property/many")
    
    data = [
        {
            'city': 'medellin', 
            'year': '2002'
        },
        {
            'id': '1234',
        },
        {
        
        }
    ]

    assert router(server_1,data[0]) == None
    assert router(server_2,data[1]) == None
    assert router(server_3,data[2]) == None