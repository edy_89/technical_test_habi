from utils import build_query_string, prune_query


def test_build_query_string():
    record_list = [
        {
            'city': 'medellin', 
            'state': 'pre_venta', 
            'year': '2002'
        },
        {
            'id': '1234',
        },
        {
            'state': 'venta',
        },
        {
            'id': '1234',
            'state': 'venta',
            'city': 'medellin', 
            'year': '2002'
        }
    ]

    query_test_list = [
        "AND PRO.city='medellin' AND STA.name='pre_venta' AND PRO.year='2002' ",
        "AND PRO.id='1234' ",
        "AND STA.name='venta' ",
        "AND PRO.id='1234' AND STA.name='venta' AND PRO.city='medellin' AND PRO.year='2002' "
    ]

    
    for count in range(len(query_test_list)):
        assert build_query_string(record_list[count]) == query_test_list[count]



def test_prune_query():
    record_list = [
        [
            {
            'id': 4,
            'address': 'calle 23 #45-67',
            'city': 'medellin',
            'label': 'Inmueble publicado en preventa',
            'price': 210000000,
            'description': '',
            'year': 2002
            },
        ],
        [
            {
            'id': 4,
            'address': 'calle 23 #45-67',
            'city': 'medellin',
            'label': 'Inmueble publicado en preventa',
            'price': 210000000,
            'description': 'Hermoso inmueble ubicado en la ciudad de medellin',
            'year': None
            }
        ],
        [
            {
            'id': None,
            'address': None,
            'city': None,
            'label': None,
            'price': None,
            'description': None,
            'year': None
            }
        ]
     ]

    query_test_list = [
        [
            {
                "id":4,
                "address":"calle 23 #45-67",
                "city":"medellin",
                "label":"Inmueble publicado en preventa",
                "price":210000000,
                "description":"no registra",
                "year":2002
            }
        ],
        [
            {
                "id":4,
                "address":"calle 23 #45-67",
                "city":"medellin",
                "label":"Inmueble publicado en preventa",
                "price":210000000,
                "description":"Hermoso inmueble ubicado en la ciudad de medellin",
                "year":"no registra"
            }
        ],
        [
            {
                "id":"no registra",
                "address":"no registra",
                "city":"no registra",
                "label":"no registra",
                "price":"no registra",
                "description":"no registra",
                "year":"no registra"
            }
        ]
    ]

    
    for count in range(len(query_test_list)):
        assert prune_query(record_list[count]) == query_test_list[count]