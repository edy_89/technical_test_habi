from components.property.store import get_records_from_db


def test_get_records_from_db():
    query_string_list = [
        "AND PRO.city='medellin' AND STA.name='pre_venta' AND PRO.year='2002' ",
        "AND PRO.id='1' ",
        "AND STA.name='en_venta' ",
        "AND PRO.id='1234' AND STA.name='en_venta' AND PRO.city='medellin' AND PRO.year='2000' "
    ]

    records_response_test_list = [
        [
            {
                'id': 4,
                'address': 'calle 23 #45-67',
                'city': 'medellin',
                'label': 'Inmueble publicado en preventa',
                'price': 210000000,
                'description': 'no registra',
                'year': 2002
            }
        ],
        [
                {
                "id": 1,
                "address": "calle 23 #45-67",
                "city": "bogota",
                "label": "Inmueble publicado en preventa",
                "price": 120000000,
                "description": "Hermoso apartamento en el centro de la ciudad",
                "year": 2000
            }
        ],
        [
            {
                "id": 2,
                "address": "carrera 100 #15-90",
                "city": "bogota",
                "label": "Inmueble publicado en venta",
                "price": 350000000,
                "description": "Amplio apartamento en conjunto cerrado",
                "year": 2011
            },
            {
                "id": 5,
                "address": "carrera 100 #15-90",
                "city": "medellin",
                "label": "Inmueble publicado en venta",
                "price": 325000000,
                "description": "Amplio apartamento en conjunto cerrado",
                "year": 2011
            },
            {
                "id": 17,
                "address": "Malabar entrada 2",
                "city": "pereira",
                "label": "Inmueble publicado en venta",
                "price": 350000000,
                "description": "Casa campestre con hermosos paisajes",
                "year": 2021
            },
            {
                "id": 18,
                "address": "Maracay casa 24",
                "city": "pereira",
                "label": "Inmueble publicado en venta",
                "price": 450000000,
                "description": "Casa campestre con sala de lujo tecnologica",
                "year": 2020
                }
        ],
        []
    ]

    
    for count in range(len(records_response_test_list)):
        assert get_records_from_db(query_string_list[count]) == records_response_test_list[count]