from network import success_response, router
from http.server import BaseHTTPRequestHandler, HTTPServer
from network import auth_header, validate_data_header


class Server(BaseHTTPRequestHandler):
    def do_HEAD(self):
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    # decorators
    @auth_header()
    @validate_data_header()
    def do_GET(self, data={}):
        router(self, data)

    @auth_header()
    def do_POST(self):
        print("ESTO ES UN POST")
        success_response(self, 'OK', {"data": "esto es una POST!"})


def main():
    port = 8000
    print(f"Listening Server On Port:{port}")
    server = HTTPServer(('', port), Server)
    server.serve_forever()


if __name__ == "__main__":
    main()
