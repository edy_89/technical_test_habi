# Prueba Técnica Habi
Habi desea tener una herramienta en la que sus usuarios puedan consultar los inmuebles
disponibles para la venta. En esta herramienta los usuarios deben ser capaces de ver tanto los
inmuebles vendidos como los disponibles. Con el objetivo de hacer más fácil la búsqueda, se
espera que los usuarios puedan aplicar diferentes filtros a la búsqueda.
Adicionalmente, se espera que los usuarios puedan darle “me gusta” a los inmuebles con el fin
de tener un ranking interno de los inmuebles más llamativos.

## Requerimientos
Habi desea tener dos microservicios. El primero para que los usuarios externos puedan
consultar los inmuebles disponibles almacenados en la base de datos. El segundo para que los
usuarios puedan darle “Me gusta” a un inmueble en específico (la solución se plantea solo de forma conceptual).

### Tecnologías
- Python
- virtualenv
- MySQL
- SQL
- Postman
- Git

# Servicio de consulta

## Abordaje del problema
La estructura propuesta para la solución de la presente prueba técnica, se basa en tres capas:
- Capa de configuración: Punto de entrada del servidor que recibe las peticiones
- Capa de red: Su tarea principal es la de redirigir las peticiones hechas al servidor y consta de dos niveles:
    - router: Es el encargado de dirigir una petición en particular hacia recursos de las capas mas internas
    - response: su labor es la de estandarizar respuestas de tipo HTTP
- Capa de componentes: Aloja elementos que constan de capas internas como su propio _network_ donde se valida la data recibida  por parte de una petición y una capa _store_ que se comunica directamente con la base de datos suministrada.

Ya que la estructura se encuentra aislada en si misma a través de capas y componentes, podemos presumir que ofrece la escalabilidad suficiente para un proyecto proporcional al tamaño de sus exigencias.

### Arquitectura de flujo organizacional

![Alt text](/diagrams/dafo.png?raw=true "Arquitectura de flujo organizacional")

# Servicio de "Me gusta"

## Abordaje del problema
De acuerdo a la base de datos proporcionada, para este segundo requerimiento tenemos el siguiente diagrama de clases:

![Alt text](/diagrams/dcp_old.png?raw=true "Diagrama de clases actual")

Si bien podemos apreciar, el modelo _authuser_ que representa al usuario, aún no tiene ninguna relación con el modelo _property_, el cual representa a los inmuebles. Teniendo en cuenta lo anterior, se propone un modelo intermedio dado que el tipo relación naciente es de _muchos a muchos_ (un usuario podrá dar likes a _muchos_ inmuebles y una propiedad tendrá likes de parte de _muchos_ usuarios):

![Alt text](/diagrams/dcp.png?raw=true "Diagrama de clases propuesto")

Así tendremos un modelo que representa con su existencia en si misma, una reacción de parte de un usuario ya sea de like o dislike. En caso que no existir esta relación, se creará por defecto una nueva con valores por defecto. De esta forma podremos validar si un usuario alguna vez a dado like, dislike o ha reaccionado a un inmueble y sus caracteristicas.

#### Definiendo el modelo like_property:

`id: ` Llave primaria

`property_id: ` Llave foránea con relación al modelo property

`auth_user_id: ` Llave foránea con relación al modelo auth_user

`date:`  Fecha de la acción like-dislike

`thumb: ` Valor booleano que indica si tenemos pulgar arriba :) o pulgar abajo :(

#### Extendiendo modelo like_property en base de datos:

```bash
    CREATE TABLE like_property (
        id INT AUTO_INCREMENT PRIMARY KEY,
        property_id INT(32) NOT NULL REFERENCES property (id) ON DELETE CASCADE,
        auth_user_id INT(32) NOT NULL REFERENCES property (id) ON DELETE CASCADE,
        thumb TINYINT(1) NOT NULL DEFAULT 1,
        created_at TIMESTAMP NOT NULL,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    );
```

#### Diagrama entidad relación

![Alt text](/diagrams/der.png?raw=true "Diagrama entidad - relacion")

### Propuesta de mejora

Como posible mejora en el modelamiento de la base de datos, se buscó eliminar al menos una tabla relacional con el fin de reducir la complejidad de las consultas. Debemos tener en cuenta cualquier operador o clausula (OR, AND, ON...) que termine involucrando mas de una tabla, obliga a que SQL procese cada componente de la clausula de forma independiente.

Como ejemplo tenemos la siguiente consulta:

```bash
    SELECT DISTINCT ALIAS.parameter1, ALIAS.parameter2
    FROM Table1 ALIAS
    INNER JOIN Table2 ALIAS2
        ON ALIAS.parameter1 = ALIAS2.parameter1
        OR ALIAS.parameter2 = ALIAS2.parameter2
```

Para SQL, el hecho de tener que hace hacer un INNER JOIN y comparar atributos respecto a el valor de la columna, implica revisar cada tabla separado e ir comparando dichos atributos.

Si observamos el siguiente diagrama propuesto, podemos inferir que la tabla status_history ya no está, y a cambio de ello existe solo una tabla llamada _status_. Esta tabla con sus nuevos atributos tiene como finalidad, poder registrar cada cambio de estado de un inmueble desde que inicia en su estado _comprando_ hasta que finaliza con un estado de _vendido_. Ésto podemos llevarlo a cabo si tenemos en cuenta que solo existen 5 posibles estados: comprando, comprado, pre_venta, en_venta y vendido.
La variable de control del estado del inmueble (_status_), al tener un número tan limitado de posibles valores podemos aprovecharla la máximo haciendo control de ella de manera funcional desde el código del backend, asi pues cada que queramos actualizar el estado de un inmueble, bastará con consultar la tabla _status_ relacionada directamente al inmueble, cambiar el valor de _status_ y actualizar el campo de tipo DATETIME que corresponda a dicho estado. 

#### Diagrama de clases propuesto

![Alt text](/diagrams/dcps.png?raw=true "Diagrama de clases propuesto solución")

# Autocorrecciones

- Registros de inmuebles que presentan valores nulos (None, Null y '')
    - Solución adoptada: Desde el punto de vista de inteligencia de negocios, presumiendo que los campos de una propiedad mas trascendentales a la hora vender una propiedad podrían ser su dirección, ciudad y precio, se decidió reemplazar aquellos valores que difieran de los 3 anteriores con un valor por defecto: "No registra".


- Cambios de estado de los inmuebles para la solución propuesta:
    - Solución adoptada: Se asume que los cambios de estado desde _comprando_ hasta _vendido_ para cada inmueble, ocurren una única vez. Dicho esto en efecto podrá registrarse cada cambio de estado con el modelo propuesto, salvo que los cambios de estado puedan regresar a estados anteriores y se requiera tener el registro de dichos cambios.


- Tablas fantasma:
    - Solución adoptada: Según la base de datos proporcionada, existen algunos registros de la tabla status_history que no se encuentran relacionados a ningún registro de la tabla property. Debido a esto se decidió incluir en la sentencia SQL, la condición de que existiera un registro de status_history para cada property y que en caso contrario no se tenga en cuenta el registro.

# Ficha Técnica

### Árbol de ficheros
```bash
    technical_test_habi/
    ├── components/
    │   ├── __init__.py   
    │   └── property/
    │       ├── __init__.py
    │       ├── network.py
    │       └── store.py
    ├── config/
    │   ├── __init__.py
    │   ├── db.py
    │   └── settings.py
    ├── diagrams/
    │   ├── dafo.png
    │   ├── dcp_old.png
    │   ├── dcp.png
    │   └── der.png
    ├── network/
    │   ├── __init__.py
    │   ├── decorators.py
    │   ├── response.py
    │   └── router.py
    ├── tests/
    │   ├── __init__.py
    │   ├── conftest.py
    │   ├── pytest.ini
    │   ├── test_funciotns/
    │   │    └── test_functions.py
    │   ├── test_network/
    │   │   └── test_network.py
    │   └── test_store/
    │       └── test_store.py       
    ├── utils/
    │    ├── __init__.py
    │    └── functions.py
    ├── .env
    ├── .gitignore
    ├── __init__.py
    ├── README.md
    ├── requirements.txt
    └── server.py
```

### Ejecutar localmenteel proyecto


- Clonar proyecto

```bash
  https://gitlab.com/edy_89/technical_test_habi.git technical_test_habi
```

- Crear un entorno virtual

```bash
  virtualenv technical_test_env
```

- Activar entorno virtual (windows)

```bash
  cd technical_test_env/Scripts
  .\activate
```

- Ir al directorio del proyecto

```bash
  cd technical_test_habi
```

- Instalar librerias

```bash
  pip install -r requirements.txt
```

- Inicializar servidor por el puerto 8000

```bash
  python server.py
```

### Peticiones REST

- Se han habilitado dos urls:

```bash
http://localhost:8000/property/many
```

```bash
http://localhost:8000/property/single
```

`many: ` Método GET que recibe a través de los queryparams los parametros de filtrado que son _city_, _state_ y _year_. Ejemplo:

  - `http://localhost:8000/property/many?city=medellin&state=pre_venta&year=2002`

  - respuesta:

  ```bash
    [
      {
          "id": 4,
          "address": "calle 23 #45-67",
          "city": "medellin",
          "label": "Inmueble publicado en preventa",
          "price": 210000000,
          "description": "no registra",
          "year": 2002
      }
    ]
  ```
  Si la uri `http://localhost:8000/property/many` llega al servidor si nigún parametro de filtrado, la respuesta del servicio rest traerá todos los inmuebles.

`single: ` Método GET que recibe a través de los queryparams el parametro _id_ para filtrar un inmueble en particular representado en la base de datos por ese _id_. Ejemplo:

  - `http://localhost:8000/property/single?id=1`

  - respuesta:

  ```bash
    [
        {
            "id": 1,
            "address": "calle 23 #45-67",
            "city": "bogota",
            "label": "Inmueble publicado en preventa",
            "price": 120000000,
            "description": "Hermoso apartamento en el centro de la ciudad",
            "year": 2000
        }
    ]
  ```
  Si la uri `http://localhost:8000/property/single` llega al servidor sin el parametro _id_ de filtrado, la respuesta del servidor arrojará un error:

  ```bash
    {
      "err_code": "NO_VALID_DATA",
      "description:": "No valid data",
      "status_code": 400
    }
  ```

#### Referencias

- https://docs.python.org/es/3/library/http.server.html
- https://ellibrodepython.com/python-pep8
